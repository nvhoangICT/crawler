# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import Select
import time
import csv

service = Service("C:/Users/Hoang/Downloads/ImageCrawler/ImageCrawler/chromedriver-win64/chromedriver-win64/chromedriver.exe")

chrome_options = Options()
prefs={"profile.managed_default_content_settings.images": 2}
chrome_options.add_experimental_option('prefs', prefs)
chrome_options.add_argument("--headless")

driver = webdriver.Chrome(options=chrome_options, service=service)
driver.get("https://fptshop.com.vn/may-tinh-xach-tay")
time.sleep(1)

select_list = driver.find_element('xpath', '/html/body/div[2]/main/div/section[2]/div/div/div/div[2]/div/select').text
select_list = select_list.split("\n")
value_list = []

for x in select_list:
    value_list.append(x[3:])

value_list.__delitem__(-1)

with open('data_vn1.csv', 'w', encoding='utf-8', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['Time', 'RON 95-III', 'change1', 'E5 RON 92-II', 'change2', 'DO 0,05S-II', 'change3', 'KO', 'change4'])
    dict_tmp = {'Xăng RON 95-III': 1, 'Xăng E5 RON 92-II': 3, 'Dầu DO 0,05S-II': 5, 'Dầu KO': 7}
    for x in value_list:
        tmp = [x, '0', '0', '0', '0', '0', '0', '0', '0']
        select = Select(driver.find_element('xpath', '/html/body/form/div[2]/main/div/section[2]/div/div/div/div[2]/div/select'))
        time.sleep(1)
        select.select_by_visible_text(x)
        time.sleep(1)
        len_tmp = len(driver.find_element('xpath', '/html/body/form/div[2]/main/div/section[2]/div/div/div/div[3]/div/table').text.split('\n'))-4
        for y in range(1,len_tmp):
            path_name = '/html/body/form/div[2]/main/div/section[2]/div/div/div/div[3]/div/table/tbody/tr[' + str(y) + ']/td[2]'
            path_price = '/html/body/form/div[2]/main/div/section[2]/div/div/div/div[3]/div/table/tbody/tr[' + str(y) + ']/td[3]'
            path_change = '/html/body/form/div[2]/main/div/section[2]/div/div/div/div[3]/div/table/tbody/tr[' + str(y) + ']/td[4]'
            name = driver.find_element('xpath', path_name).text
            if(name in dict_tmp):
                tmp[dict_tmp[name]] = driver.find_element('xpath', path_price).text
                tmp[dict_tmp[name]+1] = driver.find_element('xpath', path_change).text
            else:
                print(x)
                print(driver.find_element('xpath', path_name).text)
        writer.writerow(tmp)