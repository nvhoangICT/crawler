# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
import time
import csv

service = Service("C:/Users/Hoang/Downloads/ImageCrawler/ImageCrawler/chromedriver-win64/chromedriver-win64/chromedriver.exe")

chrome_options = Options()
prefs={"profile.managed_default_content_settings.images": 2}
chrome_options.add_experimental_option('prefs', prefs)
chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-web-security") 
chrome_options.add_argument("--disable-gpu")
chrome_options.add_argument('--log-level=1')

driver = webdriver.Chrome(options=chrome_options, service=service)
driver.get("https://fptshop.com.vn/may-tinh-xach-tay?sort=ban-chay-nhat&trang=10")
time.sleep(1)

# Create a CSV file to store the information
csv_file_path = 'product_information.csv'

# Open the CSV file in write mode
with open(csv_file_path, mode='w', newline='', encoding='utf-8') as csv_file:
    # Create a CSV writer
    csv_writer = csv.writer(csv_file)

    # Write the header row
    csv_writer.writerow(['name', 'price', 'sale', 'screen', 'cpu', 'ram', 'hardware', 'graphic', 'weight'])

    # Loop through the items and write information to the CSV file
    select_list = driver.find_elements(By.CLASS_NAME, "cdt-product")
    for i, item in enumerate(select_list):
        print(len(select_list))
        try:
            print(f"Sản phẩm thứ {i+1}: ")
            name = item.find_element(By.CLASS_NAME, "cdt-product__name").text
            print(name)
            price = item.find_element(By.CLASS_NAME, "progress").text
            print(price)
            sale = item.find_element(By.CLASS_NAME, "strike-price").text
            print(sale)
            details = item.find_elements(By.XPATH, ".//div[@class='cdt-product__config__param']/span")           
            for detail in details:
                title = detail.get_attribute('data-title')
                value = detail.text

                if title == "Màn hình":
                    screen = value
                    print(screen)
                elif title == "CPU":
                    cpu = value
                    print(cpu)
                elif title == "RAM":
                    ram = value
                    print(ram)
                elif title == "Ổ cứng":
                    hardware = value
                    print(hardware)
                elif title == "Đồ họa":
                    graphic = value
                    print(graphic)
                elif title == "Trọng lượng":
                    weight = value
                    print(weight)

            # Write the information to the CSV file
            csv_writer.writerow([name, price, sale, screen, cpu, ram, hardware, graphic, weight])

        except Exception as e:
            print(f"An error occurred: {e}")

# Close the Chrome WebDriver
driver.quit()

