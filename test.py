import requests
from bs4 import BeautifulSoup
 
url = "https://fptshop.com.vn/may-tinh-xach-tay"  # Replace with the actual URL
 
# Make a request to the website
response = requests.get(url)
 
# Check if the request was successful (status code 200)
if response.status_code == 200:
    # Parse the HTML content of the page
    soup = BeautifulSoup(response.text, 'html.parser')
 
    # Find all div elements with class "cdt-product prd-lap product-sale"
    products = soup.find_all('div', class_='cdt-product prd-lap product-sale')
 
    # Loop through each product
    for product in products:
        # Extract relevant information from the product
        product_name = product.find('a', class_='cdt-product__name').text.strip()
        product_price = product.find('div', class_='progress').text.strip()
        product_discount = product.find('div', class_='strike-price').text.strip()
 
        # You can extract more information as needed
 
        # Print or store the information
        print(f"Product Name: {product_name}")
        print(f"Product Price: {product_price}")
        print(f"Product Discount: {product_discount}")
        print("------------")
 
else:
    print(f"Failed to retrieve the page. Status code: {response.status_code}")